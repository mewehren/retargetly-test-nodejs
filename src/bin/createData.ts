var mongoose = require('mongoose');
import dotenv from 'dotenv';

import clientModel from '../API/Application/Domain/Models/Client'
import userModel from '../API/Application/Domain/Models/User'
dotenv.config();
const fs = require('fs')

const uri: string = process.env.MONGODB_URI;
console.log('entro', uri);

mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }).then(async () => {
    //don't show the log when it is test
    console.log("Connected to %s", process.env.MONGODB_URI);

    if (process.env.NODE_ENV !== "test") {
        console.log('Try to create data ...')

        let client1 = new clientModel({
            id: 'application',
            clientId: 'application',
            clientSecret: 'secret',
            grants: [
                'password',
                'refresh_token'
            ],
            redirectUris: []
        });

        let client2 = new clientModel({
            clientId: 'confidentialApplication',
            clientSecret: 'topSecret',
            grants: [
                'password',
                'client_credentials'
            ],
            redirectUris: []
        });

        let user = new userModel({
            username: 'admin',
            password: "4794157441ba7426db2659c87438996e628a67d35a94c274be428879b7c4f4a6b1d20a9809890bad5ffa60c6eeb1dbb4d515377aed6faaa410cd3280f35f392d",
            salt: "0b61ccc87441a5e551b48869ade0ed03",
        });

        try {
            await client1.save();
            await client2.save();
        } catch (error) {
            console.warn('Failed to persist Clients: ', error.message);
        }

        try {
            await user.save();
        } catch (error) {
            console.warn('Failed to persist User: ', error.message);
        }

        console.log("Data Created ...");
        process.exit();
    }
})
    .catch(err => {
        console.error("App starting error:", err.message);
        process.exit(1);
    });