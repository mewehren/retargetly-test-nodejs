/**
* @file index
* @description Starts a Service
**/
import cors from 'cors';
import bodyParser from 'body-parser';
import './API/Common/Logger';
import routes from './API/Routes/routes';
import * as corsConfiguration from './API/Config/cors';
import mongoose from 'mongoose'
import express from 'express';

const app = express();

(async () => {
  app.use(express.static('public'));

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cors(corsConfiguration));

  app.use(routes);

  app.listen(process.env.APP_PORT, () => {
    connectDBService();
  });
})();


const connectDBService = () => {
  // Create connection to the db
  const uri: string = process.env.MONGODB_URI as string
  console.log('Connecting URI', uri)
  mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }).then(() => {
    //don't show the log when it is test
    if (process.env.NODE_ENV !== "test") {

      console.log("Connected to %s", process.env.MONGODB_URI);
      console.log("App is running ... \n");
      console.log('> Ready on http://localhost:'+process.env.APP_PORT);
      console.log("Press CTRL + C to stop the process. \n");
    }
  })
    .catch(err => {
      console.error("App starting error:", err.message);
      process.exit(1);
    });

};

export default app;