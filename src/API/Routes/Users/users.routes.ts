import UserController from '../../Controllers/UserController';
import * as express from 'express';

const router = express.Router();

const userController = new UserController();

router.post('/', (req, res, next) => {
  userController.store(req, res, next);
});

export default router;
