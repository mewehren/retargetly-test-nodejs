import FileController from '../../Controllers/FileController';
import * as express from 'express';
const formidableMiddleware = require('express-formidable');

const router = express.Router();

const fileController = new FileController();

router.post('/',formidableMiddleware(), (req, res, next) => {
  fileController.store(req, res, next);
});

router.get('/', (req, res, next) => {
  fileController.getFile(req, res, next);
});

export default router;
