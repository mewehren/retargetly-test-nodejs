import * as express from 'express';
import OAuth2Middleware from '../../Middlewares/OAuth2Middleware';
import OAuth2TokenController from '../../Controllers/OAuth2TokenController';

const router = express.Router();
const oAuth2TokenController = new OAuth2TokenController();

router.post('/access_token', (req, res, next) => oAuth2TokenController.requestAccessToken(req, res, next));

router.get('/logout', OAuth2Middleware, (req, res, next) => {
  oAuth2TokenController.revokeToken(req, res, next);
});

export default router;
