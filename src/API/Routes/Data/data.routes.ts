import DataController from '../../Controllers/DataController';
import * as express from 'express';
const formidableMiddleware = require('express-formidable');

const router = express.Router();

const dataController = new DataController();

router.post('/',formidableMiddleware(), (req, res, next) => {
  dataController.store(req, res, next);
});

router.get('/', (req, res, next) => {
  dataController.getData(req, res, next);
});

export default router;
