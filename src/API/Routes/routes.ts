import express from 'express';
import { error } from '../Common/Result';
import ErrorHandler from '../Middlewares/ErrorHandler';
import _authenticationRoutes from './OAuth2/authentication.routes';
import _usersRoutes from './Users/users.routes';
import _dataRoutes from './Data/data.routes';
import _fileRoutes from './File/file.routes';
import { authenticateRequest } from '../Middlewares/OAuth2Middleware';



const router = express.Router();

// BEGINS PUBLIC ROUTES DEFINITIONS


router.use('/public', express.static(process.env.PUBLIC_DIR ? process.env.PUBLIC_DIR : `${__dirname}/../../public`));

router.use('/api/oauth2/', _authenticationRoutes);

// Authorization required
router.use(authenticateRequest);

router.use('/api/users/', _usersRoutes);
router.use('/api/data/', _dataRoutes);
router.use('/api/file/', _fileRoutes);

//   ---- ENDS PUBLIC ROUTES DEFINITIONS ----

router.use(ErrorHandler);

// No routes matched
router.use((req, res, next) => {
  res.status(404).json(
    error(
      'Page not found',
      'Sorry can not find that!',
      404,
    ),
  );
});



export default router;
