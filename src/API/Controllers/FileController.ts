
import { success } from '../Common/Result';
import CommandBus from '../Application/Commands/CommandBus';
import CreateFileCommand from '../Application/Commands/File/CreateFileCommand';
import GetFileCommand from '../Application/Commands/File/GetFileCommand';
import InvalidArgumentException from '../Application/Exceptions/InvalidArgumentException';

export default class FileController {

  constructor() {
  }

  public async store(request, response, next) {
    try {
      let body = Object.assign(request.fields, request.files);
      if(!body.file){
        throw new InvalidArgumentException('File are required');
      }

      let filePath = body.file.path;
      let fileName = body.file.name;

      const command = new CreateFileCommand({ filePath, fileName });
      const result = await CommandBus.handle(command);

      return response.status(201).json(success(result, 'File created', 201));
    } catch (e) {
      next(e);
    }
  }

  public async getFile(request, response, next) {
    try {
      let body = Object.assign(request.query); 

      const command = new GetFileCommand(body);
      const result = await CommandBus.handle(command);

      return response.download(result);
    } catch (e) {
      next(e);
    }
  }

}
