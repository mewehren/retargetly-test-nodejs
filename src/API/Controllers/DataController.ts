
import { success } from '../Common/Result';
import CommandBus from '../Application/Commands/CommandBus';
import CreateDataCommand from '../Application/Commands/Data/CreateDataCommand';
import GetDataCommand from '../Application/Commands/Data/GetDataCommand';

export default class DataController {

  constructor() {
  }

  public async store(request, response, next) {
    try {
      let body = Object.assign(request.fields, request.files); // contains non-file fields, // contains files
      let filePath = body.file.path;

      const command = new CreateDataCommand({ filePath });
      const result = await CommandBus.handle(command);

      return response.status(201).json(success(result, 'Data created', 201));
    } catch (e) {
      next(e);
    }
  }

  public async getData(request, response, next) {
    try {
      let body = Object.assign(request.query); 

      const command = new GetDataCommand(body);
      const result = await CommandBus.handle(command);

      return response.status(201).json(success(result, 'Data created', 201));
    } catch (e) {
      next(e);
    }
  }

}
