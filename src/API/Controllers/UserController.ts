
import { success } from '../Common/Result';
import CommandBus from '../Application/Commands/CommandBus';
import CreateUserCommand from '../Application/Commands/Users/CreateUserCommand';

export default class UserController {

  constructor() {
  }

  public async store(request, response, next) {
    try {
      const command = new CreateUserCommand(request.body);
      const result = await CommandBus.handle(command);

      return response.status(201).json(success(result, 'User created', 201));
    } catch (e) {
      next(e);
    }
  }

}
