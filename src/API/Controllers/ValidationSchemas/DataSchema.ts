import * as Joi from 'joi';

export const GetDataSchema = {
  sort: Joi.string().valid("asc", "desc").required(),
  sortField: Joi.string().valid("name", "segment1","segment2","segment3","segment4","platformId","clientId").required(),
  limit: Joi.number()
    .optional()
};

export const DataSchema = {
  name: Joi.string()
    .required(),
  segment1: Joi.boolean()
    .required(),
  segment2: Joi.boolean()
    .required(),
  segment3: Joi.boolean()
    .required(),
  segment4: Joi.boolean()
    .required(),
  platformId: Joi.number()
    .required(),
  clientId: Joi.number()
    .required(),
};


export const ImportDataSchema = Joi.array().items(
  Joi.object(DataSchema)
    .optional()
)
