import * as Joi from 'joi';

export const GetFileSchema = {
  Filename: Joi.string()
    .required()
};