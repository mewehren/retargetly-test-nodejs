import * as Joi from 'joi';

export const StoreUserSchema = {
  nombre: Joi.string()
    .required(),
  password: Joi.string()
    .required()
};