import oauth2 from '../Config/oauth2';
import oauth2Server from 'oauth2-server';
import { success, error } from '../Common/Result';
import JsonResult from '../Common/JsonResult';
export default class OAuth2TokenController {

  public async requestAccessToken(request, response, next) {

    const oauth = new oauth2Server(oauth2);
    const Request = oauth2Server.Request;
    const Response = oauth2Server.Response;

    if (request.body && !request.body.scope) {
      delete request.body.scope;
    }

    try {
      const token = await oauth.token(new Request(request), new Response(response));
      const user = await token.user;

      const tokenData = {
        access_token: token.accessToken,
        access_token_expires_at: token.accessTokenExpiresAt,
        refresh_token: token.refreshToken,
        refresh_token_expires_at: token.refreshTokenExpiresAt,
      };

      return response.status(200).json(
        success(
          tokenData,
          'Access Token information retrieved',
          200,
        ),
      );
    } catch (error) {
      return response.status(401).json(
        JsonResult(
          {},
          error.message,
          error.code,
        ),
      );
    }
  }

  public async revokeToken(request, response, next) {
    const user = await request.user;

    await request.token.remove();
    return response.status(200).json(success({}, 'Access token revoked', 200));
  }

}
