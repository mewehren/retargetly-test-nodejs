import * as _ from 'lodash';
import { error } from '../Common/Result';
import UnauthorizedException from '../Application/Exceptions/UnauthorizedException';
import RequiredFieldException from '../Application/Exceptions/RequiredFieldException';
import NotFoundEntityException from '../Application/Exceptions/NotFoundEntityException';
import DuplicatedEntryException from '../Application/Exceptions/DuplicatedEntryException';
import InvalidArgumentException from '../Application/Exceptions/InvalidArgumentException';
import ActionNotAllowedException from '../Application/Exceptions/ActionNotAllowedException';
import PreconditionRequiredException from '../Application/Exceptions/PreconditionRequiredException';
import SameRequestInAShortPeriod from '../Application/Exceptions/SameRequestInAShortPeriod';
import StoppedServiceException from '../Application/Exceptions/StoppedServiceException';
import ExceededNumberException from '../Application/Exceptions/ExceededNumberException';

const ER_DUP_ENTRY = 1062;
const ER_ROW_IS_REFERENCED_2 = 1451;
const ER_NO_REFERENCED_ROW_2 = 1452;

const responses = async (e, response) => {
  if (e instanceof InvalidArgumentException) {
    return await response.status(400).json(
      error(e.message, e.name, 400),
    );
  }  if (e instanceof ExceededNumberException) {
    return await response.status(400).json(
        error(e.message, e.name, 400),
    );
  } else if (e instanceof RequiredFieldException) {
    return await response.status(400).json(
        error(e.message, e.name, 400),
    );
  } else if (e instanceof SameRequestInAShortPeriod) {
    return await response.status(403).json(
      error(e.message, e.name, 403),
    );
  } else if (e instanceof UnauthorizedException) {
    return await response.status(401).json(
      error(e.message, e.name, 401),
    );
  }  else if (e instanceof NotFoundEntityException) {
    return await response.status(404).json(
      error(e.message, e.name, 404),
    );
  } else if (e instanceof ActionNotAllowedException) {
    return await response.status(405).json(
      error(e.message, e.name, 405),
    );
  } else if (e instanceof PreconditionRequiredException) {
    return await response.status(428).json(
      error(e.message, e.name, 428),
    );
  }  else if (e.errno && e.errno === ER_DUP_ENTRY) {
    const exErr = new DuplicatedEntryException(e.message);
    return await response.status(exErr.httpStatus).json(
      error(exErr.message, exErr.name, exErr.httpStatus),
    );
  } else if (e instanceof StoppedServiceException) {
    return response.status(405).json(
      error(e.message, e.name, 405),
    );
  } else if (typeof e.getHttpStatus === 'function' && e.getHttpStatus) {
    return response.status(e.getHttpStatus()).json(
        error(e.message, e.name, e.getHttpStatus()),
      );
  }else if (typeof e.statusCode !== 'undefined') {
    return response.status(e.statusCode).json(
      error(e.message, e.message, e.statusCode),
    );
  }else {
    return response.status(400).json(
      error(e.message, 'Default error', 400),
    );
  }
};

export default responses;
