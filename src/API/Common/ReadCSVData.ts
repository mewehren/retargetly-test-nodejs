
const csvParse = require('csv-parser')
const getStream = require('get-stream');
const fs = require('fs');

export async function ReadCSVData(filePath): Promise<any> {
    const parseStream = csvParse();
    const data = await getStream.array(fs.createReadStream(filePath).pipe(parseStream));
    return data;
}