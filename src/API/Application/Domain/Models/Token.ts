const mongoose = require('mongoose');

// Interface
export interface IToken extends Document {
  accessToken: String;
  accessTokenExpiresAt: Date;
  refreshToken: String;
  refreshTokenExpiresAt: Date;
  client: Object;
  user: Object;
}

const TokenSchema = new mongoose.Schema({
  accessToken: {
      type: String,
      required: true,
    },
  accessTokenExpiresAt: {
      type: Date,
      required: true,
    },
  refreshToken: {
      type: String,
      required: true,
    },
  refreshTokenExpiresAt: {
      type: Date,
      default: Date.now,
    },
  client: {
  type: Object,
},
  user: {
  type: Object,
},
});

TokenSchema.index({ refreshTokenExpiresAt: 1 }, { expireAfterSeconds: 0 });

export default mongoose.model('Token', TokenSchema);
