const mongoose = require('mongoose');

// Interface
export interface IData extends Document {
    name: String,
    segment1: Boolean,
    segment2: Boolean,
    segment3: Boolean,
    segment4: Boolean,
    platformId: Number,
    clientId: Number
};

const DataSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    segment1: {
        type: Boolean,
        required: true
    },
    segment2: {
        type: Boolean,
        required: true
    },
    segment3: {
        type: Boolean,
        required: true
    },
    segment4: {
        type: Boolean,
        required: true
    },
    platformId: {
        type: Number,
        required: true
    },
    clientId: {
        type: Number,
        required: true
    }
});

export default mongoose.model('Data', DataSchema);
