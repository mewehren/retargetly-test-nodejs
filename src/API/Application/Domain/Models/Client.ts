const mongoose = require('mongoose');

// Interface
export interface IClient extends Document {
  id: String;
  clientId: String;
  clientSecret: String;
  grants: [String];
  redirectUris: [String];
}

const ClientSchema = new mongoose.Schema({
  id: {
      type: String,
      required: false,
    },
  clientId: {
      type: String,
      required: true,
    },
  clientSecret: {
      type: String,
      required: true,
    },
  grants: {
      type: [String],
    },
  redirectUris: {
  type: [String],
},
});

export default mongoose.model('Client', ClientSchema);
