const mongoose = require('mongoose');

// Interface
export interface IUser extends Document {
  username: string;
  password: string;
  salt: string;
}

const UserSchema = new mongoose.Schema({
  username: {
      type: String,
      required: true,
    },
  password: {
      type: String,
      required: true,
    },
  salt: {
      type: String,
      required: true,
    },
  created_date: {
      type: Date,
      default: Date.now,
    },
});

UserSchema.index({ username: 1 }, { unique: true });

export default mongoose.model('User', UserSchema);
