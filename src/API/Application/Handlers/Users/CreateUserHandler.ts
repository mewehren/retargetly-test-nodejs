
import CryptoHashPasswordServices from '../../Services/Users/CryptoHashPasswordServices';
import CreateUserCommand from '../../Commands/Users/CreateUserCommand';
import * as _ from 'lodash';
import userModel from '../../Domain/Models/User'
import UsersServices from '../../Services/Users/UsersServices';


export default class CreateUserHandler {
  private cryptoHashPasswordServices: CryptoHashPasswordServices;
  private userServices: UsersServices;

  constructor() {
    this.cryptoHashPasswordServices = new CryptoHashPasswordServices();
    this.userServices = new UsersServices();
  }

  public async handle(command: CreateUserCommand) {
    const data = JSON.parse(JSON.stringify(command));
    const { nombre,password } = data;

    const passwordEncrypted = await this.cryptoHashPasswordServices.passwordHash(password);

    const user = new userModel({
      username: nombre,
      password: passwordEncrypted.hash,
      salt: passwordEncrypted.salt
    });

    await this.userServices.save(user);
    
    return user;
  }
}
