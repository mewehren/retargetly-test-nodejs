
import GetDataCommand from '../../Commands/Data/GetDataCommand';
import path from 'path';
import FileNotFoundException from '../../Exceptions/FileNotFoundException';

const fs = require('fs');

export default class CreateDataHandler {

  constructor() {
  }

  public async handle(command: GetDataCommand) {
    const { Filename: fileName } = command.getBody();
    let filesFolder = path.join(`${process.cwd()}/public`, 'files');

    const fileNamePath = filesFolder + "/" + fileName;

    if (!fs.existsSync(fileNamePath)) {
     throw new FileNotFoundException(`File: ${fileName} not found`)
    }

    return fileNamePath;
  }
}
