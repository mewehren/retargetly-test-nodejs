
import CreateFileCommand from '../../Commands/File/CreateFileCommand';
import path from 'path';
import ActionNotAllowedException from '../../Exceptions/ActionNotAllowedException';

const fs = require('fs');

export default class CreateFileHandler {

  constructor() {
  }

  public async handle(command: CreateFileCommand) {
    const filePath = command.getFilePath();

    let fileName = command.getFileName();
    let filesFolder = path.join(`${process.cwd()}/public`, 'files');

    try {

    if (!fs.existsSync(filesFolder)) {
      await fs.mkdirSync(filesFolder, { recursive: true });
    }

      await fs.renameSync(filePath, filesFolder + "/" +fileName);
    } catch (error) {
      throw new ActionNotAllowedException(error);
    }

    return { fileName };
  }
}
