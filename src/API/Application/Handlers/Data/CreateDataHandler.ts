
import CreateDataCommand from '../../Commands/Data/CreateDataCommand';
import { ReadCSVData } from '../../../Common/ReadCSVData';
import DataServices from '../../Services/Data/DataServices';
import Validator from '../../../Common/Validator';
import RequiredFieldException from '../../Exceptions/RequiredFieldException';
import { ImportDataSchema } from '../../../Controllers/ValidationSchemas/DataSchema';


export default class CreateDataHandler {
  private dataServices: DataServices;
  private validator : Validator;

  constructor() {
    this.validator = new Validator();
    this.dataServices = new DataServices();
  }

  private validate(data){
    const error = this.validator.validate(data, ImportDataSchema);
    if(error) {
      const details = this.validator.validationResult(error.details)
      throw new RequiredFieldException(this.validator.validationResult(error.details));
    }
  }

  public async handle(command: CreateDataCommand) {
    const filePath = command.getFilePath();
   
    let readCSVData = await ReadCSVData(filePath);

    // validate data, belonging to the data model
    this.validate(readCSVData);

    await this.dataServices.saveInBulk(readCSVData);    
    
    return true;
  }
}
