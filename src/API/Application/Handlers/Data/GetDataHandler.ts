
import GetDataCommand from '../../Commands/Data/GetDataCommand';
import DataServices from '../../Services/Data/DataServices';


export default class CreateDataHandler {
  private dataServices: DataServices;

  constructor() {
    this.dataServices = new DataServices();
  }

  public async handle(command: GetDataCommand) {
    const {sort,sortField,limit} = command.getBody();
      
    let result = await this.dataServices.findData(sort,sortField,limit);    
    
    return result;
  }
}
