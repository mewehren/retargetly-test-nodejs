import {
  CommandBus,
  CommandHandlerMiddleware,
  ClassNameExtractor,
  InMemoryLocator,
  HandleInflector,
  Middleware,
} from 'simple-command-bus';

import NamespaceHandlerLocator from './NamespaceHandlerLocator';
import AsyncLoggerMiddleware from './AsyncLoggerMiddleware';
import dotenv from 'dotenv';

dotenv.config();

const classNameExtractor = new ClassNameExtractor();
const locator = new NamespaceHandlerLocator(__dirname + '/../Handlers');
const handlerInflector = new HandleInflector();
const commandHandlerMiddleware = new CommandHandlerMiddleware(classNameExtractor,
  locator,
  handlerInflector);

class CommandHandlerGate extends Middleware {
  constructor() {
    super();
  }

  async execute(command, next) {
    return await next(command);
  }

}

const stack = Array<Middleware>();

stack.push(new AsyncLoggerMiddleware(console));
stack.push(new CommandHandlerGate());
stack.push(commandHandlerMiddleware);

const commandBus = new CommandBus(stack);

export default commandBus;