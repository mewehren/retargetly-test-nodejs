import { Command } from 'simple-command-bus';
import * as _ from 'lodash';
import { GetDataSchema } from '../../../Controllers/ValidationSchemas/DataSchema';
import Validator from '../../../Common/Validator';

export default class GetDataCommand extends Command {
  bodyData;
  constructor(body) {
    super();

    const validator = new Validator();
    const error = validator.validate(body, GetDataSchema);
    this.bodyData= body;
  }

  getBody() {
    return this.bodyData;
  }

}
