import { Command } from 'simple-command-bus';
import Validator from '../../../Common/Validator';
import * as _ from 'lodash';

export default class CreateDataCommand extends Command {
  filePath;
  constructor(body) {
    super();

    if (!body.filePath) throw Error('FilePath not found');
    
    _.assign(this, body);
  }

  getFilePath(){
    return this.filePath;
  }

}
