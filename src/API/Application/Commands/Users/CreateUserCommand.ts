import Validator from '../../../Common/Validator';
import { Command } from 'simple-command-bus';
import * as _ from 'lodash';
import { StoreUserSchema } from '../../../Controllers/ValidationSchemas/UserSchema';

export default class CreateUserCommand extends Command {
  private password;

  constructor(body: Object) {
    super();

    if (!body) throw Error('Body not found');

    const validator = new Validator();
    const error = validator.validate(body, StoreUserSchema);

    _.assign(this, body);
  }

  getPassword() {
    return this.password;
  }
}
