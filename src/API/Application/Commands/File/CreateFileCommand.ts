import { Command } from 'simple-command-bus';
import Validator from '../../../Common/Validator';
import * as _ from 'lodash';

export default class CreateFileCommand extends Command {
  filePath;
  fileName;
  constructor(body) {
    super();

    if (!body.filePath || !body.fileName) throw Error('FilePath not found');
    
    _.assign(this, body);
  }

  getFilePath(){
    return this.filePath;
  }

  getFileName(){
    return this.fileName;
  }

}
