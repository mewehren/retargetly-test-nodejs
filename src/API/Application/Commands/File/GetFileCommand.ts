import { Command } from 'simple-command-bus';
import * as _ from 'lodash';
import Validator from '../../../Common/Validator';
import { GetFileSchema } from '../../../Controllers/ValidationSchemas/FileSchema';

export default class GetFileCommand extends Command {
  bodyData;
  constructor(body) {
    super();

    const validator = new Validator();
    const error = validator.validate(body, GetFileSchema);
    this.bodyData= body;
  }

  getBody() {
    return this.bodyData;
  }

}
