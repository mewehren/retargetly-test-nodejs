import DuplicatedEntryException from '../../Exceptions/DuplicatedEntryException';
import UserModel from '../../Domain/Models/User'

export default class UserService {
    constructor() {
    }

    async getAll() {
        return UserModel.find();
    }

    async save(user) {

        let companyToSave = new UserModel(user);
        try {
            return await companyToSave.save();
        } catch (error) {
            console.log(error);
            throw new DuplicatedEntryException(`User with username: ${user.username} not available`);
        }


    }

}