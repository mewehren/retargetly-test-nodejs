import DuplicatedEntryException from '../../Exceptions/DuplicatedEntryException';
import DataModel, { IData } from '../../Domain/Models/Data'

export default class DataService {
    constructor() {
    }

    async getAll() {
        return DataModel.find();
    }

    async save(data:IData) {

        let entity = new DataModel(data);
        try {
            return await entity.save();
        } catch (error) {
            console.log(error);
            throw new DuplicatedEntryException(`Info duplicated`);
        }


    }

    async saveInBulk(datas:IData[]) {

        try {
            await DataModel.insertMany(datas)
        } catch (error) {
            console.log(error);
            throw new DuplicatedEntryException(`Info duplicated`);
        }


    }

    async findData(sort,sortField,limit) {

        let sortObject = {};
        sortObject[sortField] = sort == 'asc' ? 1 : - 1; 

        try {
            return await DataModel.find().sort( sortObject ).limit(limit ? Number(limit) : 0)
        } catch (error) {
            console.log(error);
        }


    }

}