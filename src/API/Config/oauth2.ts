import model from './model';

const oauth2Configuration = {
  model,
  accessTokenLifetime: 24 * 60 * 60,
  refreshTokenLifetime: (24 * 60 * 60) * 2,
};

export default oauth2Configuration;
