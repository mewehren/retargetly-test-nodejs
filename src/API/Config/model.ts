import Client from '../Application/Domain/Models/Client';
import Token from '../Application/Domain/Models/Token';
import User from '../Application/Domain/Models/User';
import CryptoHashPasswordServices from '../Application/Services/Users/CryptoHashPasswordServices';

/*
 * Methods used by all grant types.
 */

const getAccessToken = function (token, callback) {

  Token.findOne({
      accessToken: token,
    }).lean().exec((function (callback, err, token) {

      if (!token) {
          console.error('Token not found');
        }

      callback(err, token);
    }).bind(null, callback));
};

const getClient = function (clientId, clientSecret, callback) {

  Client.findOne({
      clientId,
      clientSecret,
    }).lean().exec((function (callback, err, client) {

      if (!client) {
          console.error('Client not found');
        }

      callback(err, client);
    }).bind(null, callback));
};

const saveToken = function (token, client, user, callback) {

  token.client = {
      id: client.clientId,
    };

  token.user = {
      username: user.username,
    };

  let tokenInstance = new Token(token);
  tokenInstance.save((function (callback, err, token) {

      if (!token) {
          console.error('Token not saved');
        } else {
          token = token.toObject();
          delete token.id;
          delete token.__v;
        }

      callback(err, token);
    }).bind(null, callback));
};

/*
 * Method used only by password grant type.
 */

const getUser = async function (username, password, callback) {
  const cryptoService = new CryptoHashPasswordServices();

  const user2 = await  User.findOne({
      username,
    },
                                           { username: 1, password:1, salt: 1 });

  User.findOne({
      username,
    },
                      { username: 1, password:1, salt: 1 }).lean().exec((function (callback, err, user) {

                        try {
          const isValidUser = cryptoService.validPassword(user.password, user.salt, password);

          if (!isValidUser) {
              console.error('User not found');
              return callback(err);
            }

          callback(err, user);
        } catch (error) {

        }

                      }).bind(null, callback));
};

/*
 * Method used only by client_credentials grant type.
 */

const getUserFromClient = function (client, callback) {

  Client.findOne({
      clientId: client.clientId,
      clientSecret: client.clientSecret,
      grants: 'client_credentials',
    }).lean().exec((function (callback, err, client) {

      if (!client) {
          console.error('Client not found');
        }

      callback(err, {
          username: '',
        });
    }).bind(null, callback));
};

/*
 * Methods used only by refresh_token grant type.
 */

const getRefreshToken = function (refreshToken, callback) {

  Token.findOne({
      refreshToken,
    }).lean().exec((function (callback, err, token) {

      if (!token) {
          console.error('Token not found');
        }

      callback(err, token);
    }).bind(null, callback));
};

const revokeToken = function (token, callback) {

  Token.deleteOne({
      refreshToken: token.refreshToken,
    }).exec((function (callback, err, results) {

      let deleteSuccess = results && results.deletedCount === 1;

      if (!deleteSuccess) {
          console.error('Token not deleted');
        }

      callback(err, deleteSuccess);
    }).bind(null, callback));
};

/**
 * Export model definition object.
 */

export = {
  getAccessToken,
  getClient,
  saveToken,
  getUser,
  getUserFromClient,
  getRefreshToken,
  revokeToken,
};