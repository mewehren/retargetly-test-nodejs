# RETARGETLY-API

## Installation

`sudo npm install -g yarn`
`sudo yarn global add typescript`
`sudo npm install -g tsc`
`sudo npm install -g tsc-node`

These four previous ones will be the main tools that we will use for the necessary commands.

### Requirements
	* MongoDB
	* nodejs >= 10
	* npm

### Command order to install and start for the first time

```
	sudo npm install -g yarn
	sudo yarn global add typescript
	sudo npm install -g tsc
	sudo npm install -g tsc-node
	npm install typescript @types/node
	cp .env.example .env
	vi .env # Abrir .env y colocar las credenciales de la base de datos
```

```
yarn add express
yarn add @types/express
yarn install
yarn run dev
```

### For the correct support of typescript

`npm install typescript @types/node`

### Add dependencies in a particular package

- Go to the corresponding directory example.
- Execute the command: `yarn add express`
- Check if types for typescript exist: `yarn add @ types / express`

With this the dependency must already be correctly installed.

### Available Commands

* `yarn run start`: Execute the index.ts of the project in which we are standing. (Example: about `retargetly / retargetly-api`)
* `yarn run build`: This command is executed to create the corresponding compilation of the app.

>Instead of running the TypeScript compiler every time you make a change, you can start the compiler in watch mode instead so that it recompiles every time there are changes to the TypeScript files.

For this in particular we make use of the dependency `nodemon` highly known in the nodejs community.

To leave in watch the files instead of walking compiling for each change use:
`yarn run dev` (With this command it will correctly leave us in watch mode the API being able to raise the server)


### Seeders
Execute the command:

`yarn run create-data`

By showing us the Seeders executed message, we can now cut the process.
Check the BD if everything is correctly loaded.

### Login Point 1
Data to access (All endpoints are in postman)

Api Team Collection -> Oauth2 -> Login

Headers
Authorization = Basic base64 (application: secret)

Body
grant_type: password
username: admin
password: 123456

CURL:
curl --location --request POST 'http://localhost:3000/api/oauth2/access_token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Authorization: Basic YXBwbGljYXRpb246c2VjcmV0' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'username=admin' \
--data-urlencode 'password=123456'

### GET /data Point 2

curl --location --request GET 'http://localhost:3000/api/data?sort=asc&sortField=platformId&limit=10' \
--header 'Authorization: Bearer {{access_token}}'

### POST /data Point Extra
### send a csv with the submitted data structure and persist the data in the mongo database

curl --location --request POST 'http://localhost:3000/api/data' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer {{access_token}}' \
--form 'file=@{{file_path}}'


### POST /file Point 3

curl --location --request POST 'http://localhost:3000/api/file' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer {{access_token}}' \
--form 'file=@{{file_path}}'

### GET /file Point 4

curl --location --request GET 'http://localhost:3000/api/file?Filename=info.csv' \
--header 'Authorization: Bearer {{access_token}}'

